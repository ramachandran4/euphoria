import { StaticImageData } from "next/image";

import facebook from "../../../../public/assets/icons/facebook.svg"
import instagram from "../../../../public/assets/icons/instagram.svg"
import twitter from "../../../../public/assets/icons/twitter.svg"
import linkdin from "../../../../public/assets/icons/linkdin.svg"
import play from "../../../../public/assets/icons/play-store.svg"
import app from "../../../../public/assets/icons/app-store.svg"

export type footer = {
    id: number,
    src: StaticImageData,
    title: string,
    small: string,
}

export const footer = [
    {
        id: 1,
        src: facebook,
        tittle: "Google Play",
        small: "android app on",
        span: play,
    },
    {
        id: 2,
        src: instagram,
        tittle: "App Store",
        small: "Availabe on the",
        span: app,
    },
    {
        id: 3,
        src: twitter,
    },
    {
        id: 4,
        src: linkdin,
    },
]