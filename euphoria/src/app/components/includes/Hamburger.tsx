"use client"
import Image from 'next/image'
import Link from 'next/link'
import React from 'react'
// import { picture } from '../data/picture'

const Hamburger = ({isMenuModel, setIsMenuModel }: any) => {
    return (
        <div className={isMenuModel ? "active" : "maincontainer"}>
            <div className='innercontainer flex justify-end relative'>
                <div className='overlay bg-[#000000cc] fixed w-[100vw] h-[100vh] top-0 left-0' onClick={() => setIsMenuModel(false)}></div>
                <div className='menubox fixed bg-[#FFFFFF] top-0 right-0 h-[100vh] w-[50%] max-msm:w-[100%]'>
                    <div className='topbox py-[15px] px-[15px] flex justify-end'>
                        <div className='w-[12%] max-msm:w-[14%]' onClick={() => setIsMenuModel(false)}>
                            <Image src={require("../../../../public/assets/icons/close.svg").default} width={50} height={50} alt='close' />
                        </div>
                    </div>
                    <div className='bottom'>
                        <ul className=''>
                            <li className='CoreSans-medium text-[#807D7E] block py-[10px] px-[15px] max-msm:text-center'>
                                <Link href="#" className='hover:text-[#3C4242] CoreSans-bold'>Shop</Link>
                            </li>
                            <li className='CoreSans-medium text-[#807D7E] max-msm:text-center'>
                                <Link href="#" className='hover:text-[#3C4242] CoreSans-bold block py-[10px] px-[15px]'>Men</Link>
                            </li>
                            <li className='CoreSans-medium text-[#807D7E] max-msm:text-center'>
                                <Link href="#" className='hover:text-[#3C4242] CoreSans-bold block py-[10px] px-[15px]'>Woman</Link>
                            </li>
                            <li className='CoreSans-medium text-[#807D7E] max-msm:text-center'>
                                <Link href="#" className='hover:text-[#3C4242] CoreSans-bold block py-[10px] px-[15px]'>Combos</Link>
                            </li>
                            <li className='CoreSans-medium text-[#807D7E] max-msm:text-center'>
                                <Link href="#" className='hover:text-[#3C4242] CoreSans-bold block py-[10px] px-[15px]'>Joggers</Link>
                            </li>
                        </ul>
                        {/* <ul className='flex gap-7 max-2xl:gap-3'>
                        {picture.map((picture, id) => (
                            <li className='w-[10%] px-4 py-4 bg-[#F6F6F6] rounded-xl cursor-pointer'>
                                <Image src={picture.src} width={40} height={50} alt='icons' />
                            </li>
                        ))}
                        </ul> */}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Hamburger