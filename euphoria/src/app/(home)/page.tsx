"use client";

import React from 'react'
import Header from '@/app/(home)/_components/Header'
import Spotlight from '@/app/(home)/_components/Spotlight'
import Deals from '@/app/(home)/_components/Deals'
import Arrival from '@/app/(home)/_components/Arrival'
import Categories from '@/app/(home)/_components/Categories'
import Category_women from '@/app/(home)/_components/Categories-women'
import Brands from '@/app/(home)/_components/Brands'
import LimeLight from '@/app/(home)/_components/LimeLight'
import Feedback from '@/app/(home)/_components/Feedback'
import Footer from '@/app/(home)/_components/Footer'

const page = () => {
    return (
        <main>
            <Header />
            <Spotlight />
            <Deals />
            <Arrival />
            <Categories />
            <Category_women />
            <Brands />
            <LimeLight />
            <Feedback />
            <Footer />
        </main>
    )
}

export default page