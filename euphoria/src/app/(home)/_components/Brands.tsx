import Image, { StaticImageData } from 'next/image'
import React from 'react'

//---------------images------------------

import nick from "../../../../public/assets/images/nick.svg"
import hm from "../../../../public/assets/images/h&m.svg"
import levis from "../../../../public/assets/images/levi's.svg"
import polo from "../../../../public/assets/images/us-polo.svg"
import puma from "../../../../public/assets/images/puma.svg"


export type brands = {
    id: number,
    src: StaticImageData,
}

const Brands = () => {
    const brands = [
        {
            id: 1,
            src: nick,
        },
        {
            id: 2,
            src: hm,
        },
        {
            id: 3,
            src: levis,
        },
        {
            id: 4,
            src: polo,
        },
        {
            id: 5,
            src: puma,
        },
    ]
    return (
        <div className='py-20'>
            <div className="wrapper">
                <div className='bg-[#3C4242] rounded-xl text-center pt-[43px]'>
                    <h1 className='text-[50px] CoreSans-bold mb-[25px] text-[#FFFFFF] max-msm:text-[42px] max-ss:text-[38px] max-mss:text-[34px]'>Top Brands Deal</h1>
                    <p className='text-[22px] CoreSans-light mb-[67px] text-[#FFFFFF] max-mss:text-[18px]'>Up To <span className='CoreSans-bold text-[#FBD103]'>60%</span> off on brands</p>
                    <ul className='flex items-stretch pb-[59px] justify-center gap-10 max-2xl:gap-6 max-ml:flex-wrap'>
                        {brands.map((brands, id) => (
                            <li key={id} className='py-[18px] px-[20px] bg-white rounded-xl max-2xl:px-[10px] max-ml:w-[23%] max-msm:w-[40%]'>
                                <div className="w-[90%]">
                                    <Image src={brands.src} width={75} height={100} alt='nick' />
                                </div>
                            </li>
                        ))}
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default Brands