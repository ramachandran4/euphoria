"use client"
import React from 'react'

// import "slick-carousel/slick/slick.css";
// import "slick-carousel/slick/slick-theme.css";
// import Slider from "react-slick";

const Spotlight = () => {
    // var settings = {
    //     dots: true,
    //     infinite: true,
    //     speed: 500,
    //     slidesToShow: 1,
    //     slidesToScroll: 1,
    //   };
    return (
        // <Slider {...settings}>
            <div className='bg'>
                <div className='p-[90px] max-ml:p-[70px] max-sm:p-[50px] max-mss:p-[25px]'>
                    <h3 className='mb-10 text-[32px] CoreSans-medium text-[#FFFFFF] max-ml:text-[28px] max-mdl:text-[24px] max-mdl:mb-[20px] max-md:text-[20px] max-mss:text-[18px]'>T-shirt / Tops</h3>
                    <h1 className='text-[78px] leading-none CoreSans-bold text-[rgb(255,255,255)] mb-10 max-2xl:text-[50px] max-ml:text-[55px] max-mdl:text-[45px] max-mdl:mb-[20px] max-md:text-[40px]'>Summer <br /> Value Pack</h1>
                    <p className='text-[32px] CoreSans-medium text-[#FFFFFF] mb-10 max-2xl:text-[24px] max-ml:text-[28px] max-mdl:text-[24px] max-mdl:mb-6 max-md:text-[20px] max-mss:text-[18px]'>cool / colorful / comfy</p>
                    <button className='ease-in duration-300 text-[24px] text-[#3C4242] CoreSans-bold py-[15px] px-[72px] bg-[#FFFFFF] rounded-lg max-ml:px[65px] max-mdl:px-[55px] max-sm:px-[50px] max-msm:text-[20px]'>Shop Now</button>
                </div>
            </div>
        // </Slider>
    )
}

export default Spotlight
