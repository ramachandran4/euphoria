"use client"
import Image, { StaticImageData } from 'next/image'
import React from 'react'

//-------------images------------

import shirts from "../../../../public/assets/images/shirts.png"
import tshirt from "../../../../public/assets/images/printend-tshirt.png"
import plain from "../../../../public/assets/images/plain-tshirt.png"
import polo from "../../../../public/assets/images/polo-tshirt.png"
import hoodies from "../../../../public/assets/images/hoodies-sweetshirt.png"
import jeans from "../../../../public/assets/images/jeans.png"
import activewaer from "../../../../public/assets/images/activewear.png"
import boxers from "../../../../public/assets/images/boxers.png"

export type category = {
    id: number,
    src: StaticImageData,
    title: string,
}

const Categories = () => {
    const category = [
        {
            id: 1,
            src: shirts,
            tittle: "Shirts",
            span: "Explore Now!"
        },
        {
            id: 2,
            src: tshirt,
            tittle: "Printed T-Shirts",
            span: "Explore Now!"
        },
        {
            id: 3,
            src: plain,
            tittle: "Plain T-Shirt",
            span: "Explore Now!"
        },
        {
            id: 4,
            src: polo,
            tittle: "Polo T-Shirt",
            span: "Explore Now!"
        },
        {
            id: 5,
            src: hoodies,
            tittle: "Hoodies & Sweetshirt",
            span: "Explore Now!"
        },
        {
            id: 6,
            src: jeans,
            tittle: "Jeans",
            span: "Explore Now!"
        },
        {
            id: 7,
            src: activewaer,
            tittle: "Activewear",
            span: "Explore Now!"
        },
        {
            id: 8,
            src: boxers,
            tittle: "Boxers",
            span: "Explore Now!"
        },
    ]
    return (
        <div className='py-20'>
            <div className="wrapper">
                <div className='flex gap-5 items-center mb-10'>
                   <div className='w-[5px] max-mss:w-[4px]'>
                        <Image src={require("../../../../public/assets/images/rectangle.svg").default} width={50} height={50} alt='rectangle' />
                   </div>
                   <h2 className='text-[34px] CoreSans-bold text-[#3C4242] max-ss:text-[28px] max-mss:text-[22px]'>Categories For Men</h2>
                </div>
                <ul className='flex justify-between items-center w-full flex-wrap gap-7'>
                    {category.map((category, id) => (
                        <li key={id} className='w-[23%] max-xl:w-[31%] max-ml:w-[47%] max-md:w-[47%] max-msm:w-[100%]'>
                            <div className='w-full mb-8'>
                                <Image src={category.src} width={1000} height={1000} alt='joggers' />
                            </div>
                            <div className='flex items-center justify-between gap-6 w-full cursor-pointer'>
                                <div className='[w-60%]'>
                                    <h5 className='text-[18px] CoreSans-bold text-[#3C4242]'>{category.tittle}</h5>
                                    <span className='text-[12px] text-[#7F7F7F]'>{category.span}</span>
                                </div>
                                <div className='w-8'>
                                    <div className='w-[75%]'>
                                        <Image src={require("../.././../../public/assets/icons/arrow.svg").default} width={40} height={40} alt='arrow' />
                                    </div>
                                </div>
                            </div>
                        </li>
                    ))}
                </ul>
            </div>
        </div>
    )
}

export default Categories
