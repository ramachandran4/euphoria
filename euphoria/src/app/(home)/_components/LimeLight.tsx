"use client"
import Image, { StaticImageData } from 'next/image'
import React from 'react'

//------------------images--------------

import blackshirt from "../../../../public/assets/images/black.png"
import line from "../../../../public/assets/images/line-pattern.png"
import shorts from "../../../../public/assets/images/shorts.png"
import levender from "../../../../public/assets/images/lavender.png"
import heart from "../../../../public/assets/icons/heart.svg"

export type limelight = {
    id: number,
    src: StaticImageData,
    title: string,
}

const LimeLight = () => {
    const limelight = [
        {
            id: 1,
            src: blackshirt,
            tittle: "Black Sweatshirt with ....",
            span: "Jhanvi’s  Brand",
            button: "$123.00",
            image: heart
        },
        {
            id: 2,
            src: line,
            tittle: "line Pattern Black H...",
            span: "AS’s  Brand",
            button: "$37.00",
            image: heart
        },
        {
            id: 3,
            src: shorts,
            tittle: "Black Shorts",
            span: "MM’s  Brand",
            button: "$37.00",
            image: heart
        },
        {
            id: 4,
            src: levender,
            tittle: "Levender Hoodie with ....",
            span: "Nike’s  Brand",
            button: "$119.00",
            image: heart
        },
    ]
    return (
        <div className='pb-[80px]'>
            <div className="wrapper">
                <div className='flex gap-5 items-center mb-10'>
                   <div className='w-[5px] max-mss:w-[4px]'>
                        <Image src={require("../../../../public/assets/images/rectangle.svg").default} width={50} height={50} alt='rectangle' />
                   </div>
                   <h2 className='text-[34px] CoreSans-bold text-[#3C4242] max-ss:text-[28px] max-mss:text-[22px]'>In The Limelight</h2>
                </div>
                <ul className='flex justify-between items-center w-full flex-wrap gap-7 max-2xl:items-baseline'>
                    {limelight.map((limelight, id) => (
                        <li className='w-[23%] relative max-xl:w-[31%] max-ml:w-[47%] max-msm:w-[100%]'>
                            <div className='w-full mb-8'>
                                <Image src={limelight.src} width={1000} height={1000} alt='joggers' />
                            </div>
                            <div className='w-[12%] absolute top-[5%] left-[80%] py-[10px] px-[10px] rounded-full bg-[#FFFFFF] cursor-pointer'>
                                <Image src={limelight.image} width={1000} height={1000} alt='joggers' />
                            </div>
                            <div className='flex items-center justify-between gap-6 w-full'>
                                <div className='[w-60%]'>
                                    <h5 className='text-[18px] CoreSans-bold text-[#3C4242]'>{limelight.tittle}</h5>
                                    <span className='text-[14px] text-[#7F7F7F] CoreSans-medium'>{limelight.span}</span>
                                </div>
                                <button className='p-[10px] bg-[#F6F6F6] rounded-lg text-[14px] CoreSans-bold text-[#3C4242]'>{limelight.button}</button>
                            </div>
                        </li>
                    ))}
                </ul>
            </div>
        </div>
    )
}

export default LimeLight
