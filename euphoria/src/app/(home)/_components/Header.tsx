"use client"

import Image, { StaticImageData } from 'next/image'
import React, { useState } from 'react'
// import { pages } from '@/app/components/data/pages'
import Link from 'next/link'
import Hamburger from '@/app/components/includes/Hamburger'

//---------------icons---------------
import like from "../../../../public/assets/icons/like.svg"
import user from "../../../../public/assets/icons/user.svg"
import cart from "../../../../public/assets/icons/cart.svg"

export type icons = {
    id: number,
    src: StaticImageData,
    title: string,
    description: string,
}

const Header = () => {
    const icons = [
        {
            id: 1,
            src: like,
        },
        {
            id: 2,
            src: user,
        },
        {
            id: 3,
            src: cart,
        },
    ]
    const [isMenuModel, setIsMenuModel] = useState(false);
    return (
        <header className='py-5 px-5'>
            <div className="wrapper">
                <div className='flex justify-between items-center w-full gap-12 max-xl:gap-4'>
                    <div className='flex justify-between gap-1 items-center w-[50%] max-ml:w-full'>
                        <h1 className='w-[20%] max-ml:w-[18%] max-sm:w-[20%] max-msm:w-[24%] max-ss:w-[30%]'>
                            <a href="#">
                                <Image src={require("../../../../public/assets/images/logo.svg").default} width={20} height={50} alt='logo' />
                            </a>
                        </h1>
                        <ul className='flex justify-between items-center gap-12 max-2xl:gap-8 max-xl:gap-4 max-ml:hidden'>
                        {/* {pages.map((pages, id) => (
                            <li className='CoreSans-medium text-[#807D7E] cursor-pointer'>
                                <Link href="#">{pages.title}</Link>
                            </li>
                        ))} */}
                            <li className='CoreSans-medium text-[#807D7E]'>
                                <Link href="#" className='hover:text-[#3C4242] CoreSans-bold'>
                                    Shop
                                </Link>
                            </li>
                            <li className='CoreSans-medium text-[#807D7E]'>
                                <Link href="#" className='hover:text-[#3C4242] CoreSans-bold'>
                                    Men
                                </Link>
                            </li>
                            <li className='CoreSans-medium text-[#807D7E]'>
                                <Link href="#" className='hover:text-[#3C4242] CoreSans-bold'>
                                    Woman
                                </Link>
                            </li>
                            <li className='CoreSans-medium text-[#807D7E]'>
                                <Link href="#" className='hover:text-[#3C4242] CoreSans-bold'>
                                    Combos
                                </Link>
                            </li>
                            <li className='CoreSans-medium text-[#807D7E]'>
                                <Link href="#" className='hover:text-[#3C4242] CoreSans-bold'>
                                    Joggers
                                </Link>
                            </li>
                        </ul>
                    </div>
                    <div className='w-[50%] flex justify-between max-xl:gap-4 max-ml:hidden'>
                        <div className='flex px-3 py-3 bg-[#F6F6F6] w-[60%] gap-4 rounded-xl'>
                            <div className='w-[8%] max-xl:w-[10%]'>
                                <Image src={require("../../../../public/assets/icons/search.svg").default} width={40} height={50} alt='search' />
                            </div>
                            <input type="text" placeholder='search' className='h-full w-full text-[20px] max-xl:text-[18px] CoreSans-regular'/>
                        </div>
                        <ul className='flex gap-7 max-2xl:gap-3'>
                        {icons.map((icons, id) => (
                            <li className='w-[45%] px-4 py-4 bg-[#F6F6F6] rounded-xl cursor-pointer'>
                                <Image src={icons.src} width={40} height={50} alt='icons' />
                            </li>
                        ))}
                        </ul>
                    </div>
                    <div className='hidden w-[10%] max-ml:block max-ml:w-[5%] max-sm:w-[6%] max-ss:w-[10%] max-mss:w-[10%]' onClick={() => setIsMenuModel(!isMenuModel)}>
                        <Image className='block w-full' src={require("../../../../public/assets/icons/menu.svg").default} width={50} height={50} alt='menu'/>
                    </div>
                </div>
            </div>                      
            <Hamburger isMenuModel={isMenuModel} setIsMenuModel={setIsMenuModel}/>
        </header>
    )
}

export default Header