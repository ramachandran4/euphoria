"use client"
// import { deals } from '@/app/components/data/deals'
import React from 'react'

const Deals = () => {
    return (
        <div className='py-[80px] px-[80px] max-ml:py-[85px] max-ml:px-[85px] max-mdl:py-[50px] max-mdl:px-[50px] max-msm:py-[30px] max-msm:px-[30px] max-ss:py-[22px] max-ss:px-[22px] max-mss:py-[20px] max-mss:px-[20px]'>
            <ul className='flex justify-between w-full gap-4 items-stretch max-ml:flex-wrap'>
                {/* {deals.map((deals, id) => ( */}
                    <li className='unsplash w-[50%] max-ml:w-[100%]'>
                        <div className='pt-[66px] pb-[79px] pl-[28px] max-ss:pb-[50px] max-ss:pt-[40px]'>
                            <h6 className='text-lg text-[#FFFFFF] CoreSans-bold mb-6'>Low Price</h6>
                            <h4 className='text-[34px] text-[#FFFFFF] CoreSans-bold mb-[9] max-msm:text-[28px]'>High Coziness</h4>
                            <h6 className='text-base text-[#FFFFFF] CoreSans-regular mb-10'>UPTO 50% OFF</h6>
                            <span className='text-[20px] text-[#FFFFFF] CoreSans-bold underline mt-[70px] cursor-pointer max-mss:text-[18px]'>Explore items</span>
                        </div>
                    </li>
                    <li className='unsplash-1 w-[50%] max-ml:w-[100%] rounded-lg'>
                        <div className='pt-[66px] pb-[79px] pl-[28px] max-ss:pb-[50px]  max-ss:pt-[40px]'>
                            <h6 className='text-lg text-[#FFFFFF] CoreSans-bold mb-6'>Beyoung Presents</h6>
                            <h4 className='text-[34px] text-[#FFFFFF] CoreSans-bold mb-[9] max-msm:text-[28px]'>Breezy Summer <br /> Style</h4>
                            <h6 className='text-base text-[#FFFFFF] CoreSans-regular mb-10'>UPTO 50% OFF</h6>
                            <span className='text-[20px] text-[#FFFFFF] CoreSans-bold underline mt-[70px] cursor-pointer max-mss:text-[18px]'>Explore items</span>
                        </div>
                    </li>
                {/* ))} */}
            </ul>
        </div>
    )
}

export default Deals
