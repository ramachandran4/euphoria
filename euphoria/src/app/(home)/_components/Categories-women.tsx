"use client"
import Image, { StaticImageData } from 'next/image'
import React from 'react'

//----------------images------------

import hoodies from "../../../../public/assets/images/hoodies.png"
import coats from "../../../../public/assets/images/coats.png"
import tees from "../../../../public/assets/images/tees.png"
import boxers from "../../../../public/assets/images/boxers-1.png"

export type items = {
    id: number,
    src: StaticImageData,
    title: string,
}

const Category_women = () => {
    const items = [
        {
            id: 1,
            src: hoodies,
            tittle: "Hoodies & Sweetshirt",
            span: "Explore Now!"
        },
        {
            id: 2,
            src: coats,
            tittle: "Coats & Parkas",
            span: "Explore Now!"
        },
        {
            id: 3,
            src: tees,
            tittle: "Tees & T-Shirt",
            span: "Explore Now!"
        },
        {
            id: 4,
            src: boxers,
            tittle: "Boxers",
            span: "Explore Now!"
        },
    ]
    return (
        <div className=''>
            <div className="wrapper">
                <div className='flex gap-5 items-center mb-10'>
                   <div className='w-[5px] max-mss:w-[4px]'>
                        <Image src={require("../../../../public/assets/images/rectangle.svg").default} width={50} height={50} alt='rectangle' />
                   </div>
                   <h2 className='text-[34px] CoreSans-bold text-[#3C4242] max-ss:text-[28px] max-mss:text-[22px]'>Categories For Women</h2>
                </div>
                <ul className='flex justify-between items-center w-full flex-wrap gap-7'>
                    {items.map((items, id) => (
                        <li key={id} className='w-[23%] max-xl:w-[31%] max-ml:w-[47%] max-msm:w-[100%]'>
                            <div className='w-full mb-8'>
                                <Image src={items.src} width={1000} height={1000} alt='joggers' />
                            </div>
                            <div className='flex items-center justify-between gap-6 w-full'>
                                <div className='[w-60%]'>
                                    <h5 className='text-[18px] CoreSans-bold text-[#3C4242]'>{items.tittle}</h5>
                                    <span className='text-[13px] text-[#7F7F7F]'>{items.span}</span>
                                </div>
                                <div className='w-8'>
                                    <div className='w-[75%]'>
                                        <Image src={require("../.././../../public/assets/icons/arrow.svg").default} width={40} height={40} alt='arrow' />
                                    </div>
                                </div>
                            </div>
                        </li>
                    ))}
                </ul>
            </div>
        </div>
    )
}

export default Category_women
