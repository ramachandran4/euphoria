"use client"
import Image, { StaticImageData } from 'next/image'
import React from 'react'

// import "slick-carousel/slick/slick.css";
// import "slick-carousel/slick/slick-theme.css";
// import Slider from "react-slick";

//------------images----------------

import floyd from "../../../../public/assets/images/floyd.svg"
import ronald from "../../../../public/assets/images/ronald.svg"
import savannah from "../../../../public/assets/images/savannah.svg"
import stars from "../../../../public/assets/icons/stars.svg"
import star from "../../../../public/assets/icons/star-1.svg"
import star_1 from "../../../../public/assets/icons/star-2.svg"


export type feedback = {
    id: number,
    src: StaticImageData,
    title: string,
    description: string,
}

const Feedback = () => {
    // var settings = {
    //     dots: true,
    //     infinite: true,
    //     speed: 500,
    //     slidesToShow: 1,
    //     slidesToScroll: 1,
    // };
    const feedback = [
        {
            id: 1,
            src: floyd,
            span: stars,
            heading: "Floyd Miles",
            description: "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet. Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.",
            
        },
        {
            id: 2,
            src: ronald,
            span: star,
            heading: "Ronald Richards",
            description: "ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.",
        },
        {
            id: 3,
            src: savannah,
            span: star_1,
            heading: "Savannah Nguyen",
            description: "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet. Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.",
        },
    ]
    return (
        <div className='pb-[80px] max-mss:pb-[40px]'>
            <div className="wrapper">
                <div className='flex gap-5 items-center mb-10'>
                   <div className='w-[5px] max-mss:w-[4px]'>
                        <Image src={require("../../../../public/assets/images/rectangle.svg").default} width={50} height={50} alt='rectangle' />
                   </div>
                   <h2 className='text-[34px] CoreSans-bold text-[#3C4242] max-ss:text-[28px] max-mss:text-[22px]'>Feedback</h2>
                </div>
                <ul className='flex justify-between gap-10 w-full max-ml:flex-wrap'>
                    {feedback.map((feedback, id) => (
                        // <Slider {...settings}>
                            <li key={id} className='p-[23px] border-solid border-2 border-[#BEBCBD] rounded-[10px] w-[40%] max-ml:w-[47%] max-md:w-[46%] max-msm:w-[100%]'> 
                                <div className='flex justify-between items-start mb-5'>
                                    <div className='w-[20%]'>
                                        <Image src={feedback.src} width={60} height={60} alt='floyd' />
                                    </div>
                                    <div className='w-[35%] max-ml:w-[40%]'>
                                        <Image src={feedback.span} width={60} height={60} alt='stars' />
                                    </div>
                                </div>
                                <h4 className='text-[22px] text-[#3C4242] CoreSans-medium mb-5'>{feedback.heading}</h4>
                                <p className='text-[14px] text-[#807D7E] CoreSans-regular w-[95%]'>{feedback.description}</p>
                            </li>
                        // </Slider>
                    ))}
                </ul>
            </div>
        </div>
    )
}

export default Feedback