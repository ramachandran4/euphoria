"use client";

import { footer } from '@/app/components/data/footer'
import Image, { StaticImageData } from 'next/image'
import React from 'react'

//----------------images------------

import play from "../../../../public/assets/icons/play-store.svg"
import app from "../../../../public/assets/icons/app-store.svg"

export type web = {
    id: number,
    src: StaticImageData,
    title: string,
    small: string,
}

const Footer = () => {
    const web = [
        {
            id: 1,
            tittle: "Google Play",
            small: "android app on",
            span: play,
        },
        {
            id: 2,
            tittle: "App Store",
            small: "Availabe on the",
            span: app,
        },
    ]

    const sections = [
        {
            title: 'Need Help',
            links: [
                { text: 'Contact Us', href: '#' },
                { text: 'Track Order', href: '#' },
                { text: 'Returns & Refunds', href: '#' },
                { text: 'FAQ\'s', href: '#' },
                { text: 'Career', href: '#' },
            ],
        },
        {
            title: 'Company',
            links: [
                { text: 'About Us', href: '#' },
                { text: 'euphoria Blog', href: '#' },
                { text: 'euphoriastan', href: '#' },
                { text: 'Collaboration', href: '#' },
                { text: 'Media', href: '#' },
            ],
        },
        {
            title: 'More Info',
            links: [
                { text: 'Term and Conditions', href: '#' },
                { text: 'Privacy Policy', href: '#' },
                { text: 'Shipping Policy', href: '#' },
                { text: 'Sitemap', href: '#' },
            ],
        },
        {
            title: 'Location',
            links: [
                { text: 'support@euphoria.in', href: '#' },
                { text: 'Eklingpura Chouraha, Ahmedabad Main Road', href: '#' },
                { text: '(NH 8- Near Mahadev Hotel) Udaipur, India- 313002', href: '#' },
            ],
        },
    ];
    return (
        <footer className='pt-[80px] pb-[40px] bg-[#3C4242]'>
            <div className="wrapper">
                <div className='w-[90%] m-auto'>
                <ul className='flex justify-center mb-10 max-lg:justify-between max-lg:flex-wrap max-lg:gap-8 max-mdl:gap-0'>
                    {sections.map((section, index) => (
                        <li key={index} className='w-[30%] max-mdl:mb-[30px] max-msm:w-[48%] max-mss:w-[90%]'>
                            <h4 className='text-[28px] text-[#FFFFFF] CoreSans-bold mb-4 max-lg:text-[24px] max-msm:text-[22px]'>{section.title}</h4>
                            {section.links.map((link, linkIndex) => (
                                <h6 key={linkIndex} className='text-[18px] text-[#FFFFFF] CoreSans-regular mb-[5px] max-msm:text-[16px]'>
                                <a href={link.href}>{link.text}</a>
                                </h6>
                            ))}
                        </li>
                    ))}
                </ul>
                    <div className='flex justify-between items-center max-md:flex-wrap'>
                        <div className='flex gap-4 max-md:mb-[18px]'>
                            {footer.map((footer, id) => (
                                <div className='w-[25%] py-[15px] h-[45px] px-[18px] bg-white rounded-xl'>
                                    <a href="#"><Image src={footer.src} width={50} height={50} alt='facebook' /></a>
                                </div>
                            ))}
                        </div>
                        <div className='w-[29%] mb-[40px] max-2xl:w-[35%] max-xl:w-[45%] max-lg:w-[50%] max-mdl:w-[52%] max-md:w-[62%] max-sm:w-[70%] max-msm:w-[82%] max-ss:w-[98%]'>
                            <h2 className='text-[28px] text-[#FFFFFF] CoreSans-bold mb-4 text-start max-xl:text-[25px] max-msm:text-[22px]'>Download The App</h2>
                                <div className='flex items-center justify-end gap-6 w-full max-mss:flex-wrap'>
                                {web.map((web, id) => (
                                    <div className='flex gap-5 w-[50%] py-[9px] px-[9px] bg-[#404040] rounded-lg max-lg:gap-2 max-mss:gap-[10%] max-mss:w-full'>
                                        <div className='w-[15%] max-xl:w-[13%] max-mss:w-[8%]'>
                                            <Image src={web.span} width={60} height={60} alt='play-store' />
                                        </div>
                                        <div>
                                            <small className='text-[10px] text-[#FFFFFF] CoreSans-regular'>{web.small}</small>
                                            <h6 className='text-[18px] text-[#FFFFFF] CoreSans-regular cursor-pointer'>{web.tittle}</h6>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <hr className='border-solid border-[-1px] border-[#BEBCBD] mb-7'/>
                    <div className='flex justify-between'>
                        <h1 className='mb-7 pl-[78px] text-[28px] text-[#FFFFFF] CoreSans-bold max-2xl:text-[26px] max-xl:text-[25px] max-md:pl-[55px] max-md:text-[20px] max-msm:pl-[16px] max-ss:text-[16px]'>Popular Categories</h1>
                        <div className='w-[7%] pr-[78px] mb-7 cursor-pointer max-2xl:w-[9%] max-xl:w-[11%] max-ml:w-[13%] max-mdl:w-[14%] max-md:w-[17%] max-sm:w-[18%] max-msm:w-[22%] max-ss:w-[25%] max-mss:w-[29%]'>
                            <Image src={require("../../../../public/assets/icons/dropdown.svg").default} width={40} height={40} alt='dropdown' />
                        </div>
                    </div>
                    <hr className='border-solid border-[-1px] border-[#BEBCBD] mb-[37px]'/>
                    <p className='text-[18px] text-[#FFFFFF] CoreSans-bold text-center max-mss:text-[15px]'>Copyright © 2023 Euphoria Folks Pvt Ltd. All rights reserved.</p>
                </div>
            </div>
        </footer>
    )
}

export default Footer