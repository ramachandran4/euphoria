import { StaticImageData } from "next/image";
import Image from 'next/image'
import React from 'react'

//-------images----------------

import joggers from "../../../../public/assets/images/joggers.png"
import sleeve from "../../../../public/assets/images/full-sleeve.png"
import tshirt from "../../../../public/assets/images/tshirt.png"
import urbon from "../../../../public/assets/images/urbon-shirts.png"

export type arrival = {
    id: number,
    src: StaticImageData,
    title: string,
}

const Arrival = () => {
    const arrival = [
        {
            id: 1,
            src: joggers,
            tittle: "Knitted Joggers",
        },
        {
            id: 2,
            src: sleeve,
            tittle: "Full Sleeve",
        },
        {
            id: 3,
            src: tshirt,
            tittle: "Active T-Shirts",
        },
        {
            id: 4,
            src: urbon,
            tittle: "Urban Shirts",
        },
    ]
    return (
        <div>
            <div className='wrapper'>
                <div className='flex gap-5 items-center mb-10'>
                   <div className='w-[5px]'>
                        <Image src={require("../../../../public/assets/images/rectangle.svg").default} width={50} height={50} alt='rectangle' />
                   </div>
                   <h2 className='text-[34px] CoreSans-bold text-[#3C4242]'>New Arrival</h2>
                </div>
                <ul className='flex justify-between items-center w-full mb-24 max-ml:flex-wrap max-mss:mb-8'>
                    {arrival.map((arrival, id) => (
                        <li className='w-[23%] max-ml:w-[48%] max-ss:w-[100%]'>
                            <div className='w-full mb-8'>
                                <Image src={arrival.src} width={262.81} height={1000} alt='joggers' />
                            </div>
                            <h4 className='text-xl CoreSans-bold text-[#3C4242]'>{arrival.tittle}</h4>
                        </li>
                    ))}
                </ul>
                <div className='flex w-full pb-0 max-ml:flex-wrap'>
                    <div className='view w-[50%] pt-[100px] px-[74px] pb-[100px] max-xl:px-[55px] max-xl:pt-[120px] max-xl:pb-[120px] max-ml:w-[100%] max-msm:pt-[115px] max-msm:pb-[120px] max-msm:px-[35px] max-mss:pt-[55px] max-mss:rounded-[10px]'>
                        <h1 className='text-[34px] CoreSans-bold text-[#FFFFFF] mb-[30px] max-mss:text-[28px]'>WE MADE YOUR EVERYDAY FASHION BETTER!</h1>
                        <p className='text-xl CoreSans-light text-[#FFFFFF] w-[80%] mb-[50px] max-xl:text-w-[97%] max-msm:w-[100%]'>In our journey to improve everyday fashion, euphoria presents EVERYDAY wear range - Comfortable & Affordable fashion 24/7</p>
                        <button className='text-lg text-[#3C4242] py-3 px-11 bg-[#ffffff] CoreSans-bold rounded-lg'>Shop Now</button>
                    </div>
                    <div className='pose w-[50%] max-ml:w-[100%] max-mss:rounded-[10px]'>
                        <Image src={require("../../../../public/assets/images/poses.png")} width={1000} height={1000} alt='poses' />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Arrival
